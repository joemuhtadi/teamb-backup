const sequelize = require("../config/sequelize.config");
const { Sequelize } = require('sequelize');

const this_table_name = 'users';

console.log("[SEQUELIZE] Use "+this_table_name);

const UserModel = sequelize.define('UserModel', {
  id: {
    type: Sequelize.UUID,
    primaryKey: true
  },
  fullname: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false
  },
  profile_picture: {
    type: Sequelize.STRING
  }
}, {
  tableName: this_table_name,
  timestamps: true,
  indexes: [ { unique: false, fields: ['fullname'] } ]
})

module.exports = UserModel