const sequelize = require("../config/sequelize.config");
const { Sequelize } = require('sequelize');

const this_table_name = 'course_view';

console.log("[SEQUELIZE] Use "+this_table_name);

const CourseViewModel = sequelize.define('CourseViewModel', {
  courseId: {
    type: Sequelize.UUID,
    allowNull: false,
    references: {
      model: {
        tableName: 'courses'
      },
      key: 'id'
    }
  },
  userId: {
    type: Sequelize.UUID,
    allowNull: false,
    references: {
      model: {
        tableName: 'users'
      },
      key: 'id'
    }
  }
}, {
  tableName: this_table_name,
  timestamps: false,
  paranoid: false,
  indexes: [ { unique: true, fields: ['courseId', 'userId'] }, { unique: false, fields: ['userId'] }, { unique: false, fields: ['courseId'] } ]
})

module.exports = CourseViewModel