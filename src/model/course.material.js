const sequelize = require("../config/sequelize.config");
const { Sequelize } = require('sequelize');

const this_table_name = 'course_material';

console.log("[SEQUELIZE] Use "+this_table_name);

const CourseMaterialModel = sequelize.define('CourseMaterialModel', {
  id : {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  courseId: {
    type: Sequelize.UUID,
    allowNull: false,
    references: {
      model: {
        tableName: 'courses'
      },
      key: 'id'
    }
  },
  type: {
    type: Sequelize.ENUM('pdf', 'video'),
    allowNull: false
  },
  overview: {
    type: Sequelize.TEXT
  },
  path: {
    type: Sequelize.STRING
  }
}, {
  tableName: this_table_name,
  timestamps: true,
  paranoid: true,
  indexes: [ { unique: false, fields: ['courseId'] } ]
})

module.exports = CourseMaterialModel