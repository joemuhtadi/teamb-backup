const sequelize = require("../config/sequelize.config");
const { Sequelize } = require('sequelize');

const this_table_name = 'courses';

console.log("[SEQUELIZE] Use "+this_table_name);

const CourseModel = sequelize.define('CourseModel', {
  id: {
    type: Sequelize.UUID,
    allowNull: false,
    primaryKey: true
  },
  categoryId: {
    type: Sequelize.INTEGER,
    allowNull: false,
    references: {
      model: {
        tableName: 'course_category'
      },
      key: 'id'
    }
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  desc: {
    type: Sequelize.TEXT
  },
  picture: {
    type: Sequelize.STRING
  },
}, {
  tableName: this_table_name,
  timestamps: true,
  paranoid: true,
})

module.exports = CourseModel