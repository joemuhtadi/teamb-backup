const sequelize = require("../config/sequelize.config");
const { Sequelize } = require('sequelize');

const this_table_name = 'user_course';

console.log("[SEQUELIZE] Use "+this_table_name);

const UserCourseModel = sequelize.define('UserCourseModel', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  userId: {
    type: Sequelize.UUID,
    allowNull: false,
    references: {
      model: {
        tableName: 'users'
      },
      key: 'id'
    }
  },
  courseId: {
    type: Sequelize.UUID,
    allowNull: false,
    references: {
      model: {
        tableName: 'courses'
      },
      key: 'id'
    }
  },
  isCompleted: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
  },
  mark: {
    type: Sequelize.INTEGER,
  },
  rating: {
    type: Sequelize.INTEGER
  }
}, {
  tableName: this_table_name,
  timestamps: true,
  paranoid: true,
  indexes: [ { unique: false, fields: ['userId'] }, { unique: false, fields: ['courseId'] } ]
  
})

module.exports = UserCourseModel