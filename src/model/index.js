const UserModel = require('./user');
const CourseModel = require('./course');

const UserWishlistModel = require('./user.wishlist');
const UserCourseModel = require('./user.course');

const CourseCategoryModel = require('./course.category')
const CourseMaterialModel = require('./course.material')
const CourseExamModel = require('./course.exam')
const CourseViewModel = require('./course.view')
const CourseLikeModel = require('./course.like')

UserModel.hasMany(UserWishlistModel, {
  foreignKey: 'userId'
})
UserModel.hasMany(UserCourseModel, {
  foreignKey: 'userId'
})
CourseModel.belongsTo(CourseCategoryModel, {
  foreignKey: 'categoryId'
});
CourseModel.hasMany(CourseMaterialModel, {
  foreignKey: 'courseId'
})
CourseModel.hasMany(CourseExamModel, {
  foreignKey: 'courseId'
})
CourseModel.hasMany(CourseViewModel, {
  foreignKey: 'courseId'
})
CourseModel.hasMany(CourseLikeModel, {
  foreignKey: 'courseId'
})

module.exports = {
  UserModel,
  UserWishlistModel,
  UserCourseModel,
  CourseCategoryModel,
  CourseModel,
  CourseCategoryModel,
  CourseMaterialModel,
  CourseExamModel,
  CourseViewModel,
  CourseLikeModel,
}