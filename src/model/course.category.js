const sequelize = require("../config/sequelize.config");
const { Sequelize } = require('sequelize');

const this_table_name = 'course_category';

console.log("[SEQUELIZE] Use "+this_table_name);

const CourseCategoryModel = sequelize.define('CourseCategoryModel', {
  id : {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING
  }
}, {
  tableName: this_table_name,
  timestamps: false
})

module.exports = CourseCategoryModel