const sequelize = require("../config/sequelize.config");
const { Sequelize } = require('sequelize');

const this_table_name = 'course_exam';

console.log("[SEQUELIZE] Use "+this_table_name);

const CourseExamModel = sequelize.define('CourseExamModel', {
  id : {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  courseId: {
    type: Sequelize.UUID,
    allowNull: false,
    references: {
      model: {
        tableName: 'courses'
      },
      key: 'id'
    }
  },
  questionId : {
    type: Sequelize.INTEGER,
    allowNull: false
  },
  type: {
    type: Sequelize.ENUM('multiple', 'single'),
    allowNull: false
  },
  question: {
    type: Sequelize.STRING,
    allowNull: false
  },
  choices: {
    type: Sequelize.JSON,
    allowNull: false
  },
  answer: {
    type: Sequelize.ARRAY(Sequelize.STRING),
    allowNull: false
  },
}, {
  tableName: this_table_name,
  timestamps: true,
  paranoid: true,
  indexes: [ { unique: false, fields: ['courseId'] } ]
})

module.exports = CourseExamModel