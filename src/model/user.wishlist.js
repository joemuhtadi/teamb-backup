const sequelize = require("../config/sequelize.config");
const { Sequelize } = require('sequelize');

const this_table_name = 'user_wishlist';

console.log("[SEQUELIZE] Use "+this_table_name);

const UserWishlistModel = sequelize.define('UserWishlistModel', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  userId: {
    type: Sequelize.UUID,
    allowNull: false,
    references: {
      model: {
        tableName: 'users'
      },
      key: 'id'
    }
  },
  courseId: {
    type: Sequelize.UUID,
    allowNull: false,
    references: {
      model: {
        tableName: 'courses'
      },
      key: 'id'
    }
  }
}, {
  tableName: this_table_name,
  timestamps: true,
  paranoid: true,
  indexes: [ { unique: false, fields: ['userId'] }, { unique: false, fields: ['courseId'] } ]
})

module.exports = UserWishlistModel