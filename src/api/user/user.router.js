const express = require('express');
const router = express.Router();

const responseHandler = require('../../middleware/response.handler');

const UserController = require('./user.controller');
const userController = new UserController();

router.post('/signup', userController.register);
router.post('/login', userController.login);
router.use(responseHandler);

module.exports = router
