const CustomError = require('../../helper/error.class');
const FormValidation = require('../../helper/form.validation');
const { UserModel } = require('../../model');
const { Op } = require("sequelize");
const { v4: uuidv4, v5: uuidv5 } = require('uuid');
const { hashSync, genSaltSync, compareSync } = require("bcrypt");
const { sign } = require("jsonwebtoken");
class UserController {

  async register(req,res,next) {
    try {
      const postdata = req.body;
      const formValidation = new FormValidation();
      const isValid = await formValidation.signUpValidator(postdata);
      if (!isValid.status) {
        throw new CustomError({
          status: 422,
          entity: 'signUp',
          body: {
            error: isValid.error
          }
        });
      }
      const isExist = await UserModel.findAll({ where: {
        [Op.or]: [
          { username: { [Op.eq]: postdata.username } },
          { email: { [Op.eq]: postdata.email } }
        ]
      }});
      if (isExist.length !== 0) {
        throw new CustomError({
          status: 422,
          entity: 'signUp',
          body: {
            error: {
              name: 'account-used',
              message: 'Username or Email has been used!'
            }
          }
        });
      } else {
        const salt = genSaltSync(10);
        const hashedPassword = await hashSync(postdata.password, salt);
        const generatedUUID = await uuidv5(postdata.username, process.env.APP_UUID_NAMESPACE);
        const user = {
          id: generatedUUID,
          fullname: postdata.fullname,
          username: postdata.username,
          email: postdata.email,
          password: hashedPassword
        }
        const createdUser = await UserModel.create(user);
        if (!createdUser) {
          throw new CustomError({
            status: 500,
            entity: 'signUp',
            body: {
              error: {
                name: 'internal-server-error',
                message: 'Internal Server Error'
              }
            }
          });
        }
        const jsonWebToken = sign({ id: createdUser.id }, process.env.APP_JWT_KEY, { expiresIn: process.env.APP_JWT_EXPIRE });
        res.locals.responseLogic = {
          status: 201, 
          entity: 'signup',
          body: {
            data: {
              fullname: createdUser.fullname,
              username: createdUser.username,
              email: createdUser.email,
              token: jsonWebToken
            }
          }
        }
        next();
      }
    } catch (error) {
      next(error)
    }
  }

  async login(req,res,next) {
    try {
      const postdata = req.body;
      if ( (!postdata.username && !postdata.email) || !postdata.password ) {
        throw new CustomError({
          status: 422,
          entity: 'login',
          body: {
            error: {
              name: 'data-incomplete',
              message: 'All fields is required!'
            }
          }
        });
      }
      let param = {};
      if (postdata.username) {
        param = {
          username: postdata.username
        }
      } else {
        param = {
          email: postdata.email
        }
      }
      const user = await UserModel.findOne({ where: param });      
      if (!user) {
        throw new CustomError({
          status: 401,
          entity: 'login',
          body: {
            error: {
              name: 'no-user-found',
              message: 'User does not exist!'
            }
          }
        });
      }
      const isPasswordValid = await compareSync(postdata.password, user.password);
      if (!isPasswordValid) {
        throw new CustomError({
          status: 401,
          entity: 'login',
          body: {
            error: {
              name: 'wrong-password',
              message: 'Wrong Password!'
            }
          }
        });
      }
      const jsontoken = sign({ id: user.uuid }, process.env.APP_JWT_KEY, { expiresIn: process.env.APP_JWT_EXPIRE });
      res.locals.responseLogic = {
        status: 200, 
        entity: 'login',
        body: {
          data: {
            fullname: user.fullname,
            username: user.username,
            email: user.email,
            token: jsontoken
          }
        }
      }
      next();
    } catch (error) {
      next(error);
    }
  }

}

module.exports = UserController