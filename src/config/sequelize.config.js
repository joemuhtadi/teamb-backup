const path = require('path')
const { Sequelize } = require('sequelize');

const sqlLogging = process.env.NODE_ENV === 'production' ? false : process.env.APP_SEQUELIZE_LOGGING === 'false' ? false : true;

const dialectOptions = process.env.APP_POSTGRES_SSL === 'true' ? {
  ssl: {
    sslmode: 'verify-ca',
    sslrootcert: path.resolve(__dirname+'/ca.pem')
  }
} : {};

const sequelize = new Sequelize(process.env.APP_POSTGRES_DATABASE, process.env.APP_POSTGRES_USER, process.env.APP_POSTGRES_PASSWORD, {
  host: process.env.APP_POSTGRES_HOST,
  port: process.env.APP_POSTGRES_PORT,
  native: true,
  ssl: true,
  dialect: 'postgres',
  dialectOptions: dialectOptions,
  logging: sqlLogging //false when production
});

try {
  sequelize.authenticate();
  sequelize.sync(); // auto generate table if not exists (migration)
  console.log('[SEQUELIZE] Connection has been established successfully.');
} catch (error) {
  console.error(error);
  console.error('[SEQUELIZE] Unable to connect to the database:', error);
}

module.exports = sequelize;