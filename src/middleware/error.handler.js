module.exports = (err, req, res, next) => {
  if (err.name === 'OperationalError') {
    // THIS IS OPERATIONAL ERROR
    res.locals.error = err.error;
    return res.status(err.error.status).json(err.error.body);
  } else {
    // THIS IS PROGRAMMER ERROR (BUGS)
    console.log("\x1b[36m"+err.name+"\x1b[0m")
    console.log("\x1b[36m"+err.message+"\x1b[0m");
    console.log(err);
    res.locals.error = err;
    return res.status(500).json({ error: { name:'interal-server-error', message: 'Internal Server Error' }});
  }
};