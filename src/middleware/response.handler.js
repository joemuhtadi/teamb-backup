module.exports = async (req, res) => {
  const status = res.locals.responseLogic.status;
  const body = res.locals.responseLogic.body;
  return res.status(status).json(body);
}