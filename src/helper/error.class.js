class CustomError extends Error {
  constructor(error, ...params) {
    super(...params)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, CustomError)
    }
    this.error = error
    this.name = 'OperationalError'
  }
}
module.exports = CustomError;