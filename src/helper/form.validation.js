class FormValidation {
  async signUpValidator(postdata) {
    try {
      let result = {
        status: false
      }
      if (!postdata.fullname || !postdata.username || !postdata.email || !postdata.password ) {
        result.error = {
          name: 'data-incomplete',
          message: 'All fields is required!'
        }
        return result
      } else {
        const aplhaNumeric = /^[a-zA-Z0-9]+$/;
        const alphaSpace = /^[a-zA-Z\s]+$/;
        const email = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        const nonWhiteSpace = /^[\S]+$/;
        let formErrors = [];
        if (!aplhaNumeric.test(postdata.username)) formErrors.push({ field: 'username', message: 'accept-only-aplha-numeric'})
        if (!alphaSpace.test(postdata.fullname)) formErrors.push({ field: 'fullname', message: 'accept-only-aplha-space'})
        if (!email.test(postdata.email)) formErrors.push({ field: 'email', message: 'accept-only-email-type'})
        if (!nonWhiteSpace.test(postdata.password)) formErrors.push({ field: 'password', message: 'accept-only-non-whitespace'}) 
        if (formErrors.length !== 0) {
          result.error = {
            name: 'data-not-acceptable',
            message: 'Please input a valid forms',
            errors: formErrors
          }
        } else {
          result.status = true
        }
        return result;
      }
    } catch (error) {
      next(error);
    }
  }
}

module.exports = FormValidation;