// initialize dependencies
const express = require('express');
const app = express();
const path = require('path')
const bodyParser = require('body-parser');
require('dotenv').config();
global.baseDir = path.resolve(__dirname);

require('./src/model');

// initialize API middleware
app.use(bodyParser.json());
app.use(require(baseDir+'/src/middleware/cors.middleware'));

// define app routing
app.use('/api/user', require(baseDir+'/src/api/user/user.router'));
// app.use('/api/course', require(baseDir+'/src/api/course/course.router'));

// error handler  middleware
app.use(require(baseDir+'/src/middleware/error.handler'));

// run app
app.listen(process.env.APP_PORT || 3000, () => {
  console.log("")
  console.log(`\x1b[36m================================================================\x1b[0m`)
  console.log(`\x1b[36m============= Server running on port ${process.env.APP_PORT || 3000} ======================\x1b[0m`)
  console.log(`\x1b[36m================================================================\x1b[0m`)
  console.log("")
});